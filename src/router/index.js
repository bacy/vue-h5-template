import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import appRoutes from './app'
import module1Route from './module1'
import module2Route from './module2'

Vue.use(VueRouter)

export const routes = [
  module1Route,
  module2Route,
  ...appRoutes
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  const user = store.state.app.userInfo
  if (!user.uid && to.path !== '/login') {
    next('/login')
  } else {
    next()
  }
})

export default router
