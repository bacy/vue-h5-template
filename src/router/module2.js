const module2Route = {
  path: '/module2',
  name: 'module2',
  meta: { title: '菜单2', isNav: true },
  component: () => import('../views/layout'),
  redirect: 'module1/sub1',
  children: [
    {
      path: 'sub1',
      name: 'module2Sub1',
      meta: { title: '模块1', isNav: true },
      component: () => import('../views/module2/sub1')
    },
    {
      path: 'sub2',
      name: 'module2Sub2',
      meta: { title: '模块2', isNav: true },
      component: () => import('../views/module2/sub2')
    }
  ]
}
export default module2Route
