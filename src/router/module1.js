const module1Route = {
  path: '/module1',
  name: 'module1',
  meta: { title: '菜单1', isNav: true },
  component: () => import('../views/layout'),
  redirect: 'module1/sub1',
  children: [
    {
      path: 'sub1',
      name: 'module1Sub1',
      meta: { title: '模块1', isNav: true },
      component: () => import('../views/module1/sub1')
    },
    {
      path: 'sub2',
      name: 'module1Sub2',
      meta: { title: '模块2', isNav: true },
      component: () => import('../views/module1/sub2')
    }
  ]
}
export default module1Route
