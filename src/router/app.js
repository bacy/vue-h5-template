const appRoutes = [
  {
    path: '/',
    meta: { title: '首页' },
    component: () => import('../views/layout'),
    redirect: 'home',
    children: [
      {
        name: 'home',
        path: '/home',
        meta: { title: '首页' },
        component: () => import('../views/home')
      }
    ]
  },
  {
    path: '/login',
    meta: { title: '登录' },
    component: () => import('../views/login')
  },
  {
    path: '/404',
    meta: { title: '404' },
    component: () => import('../views/error/404')
  },
  { path: '/*', redirect: '/404' }
]
export default appRoutes
