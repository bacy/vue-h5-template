import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import modules from './modules'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules,
  plugins: [createPersistedState({
    key: 'hz_db',
    storage: window.localStorage,
    reducer(state) {
      return {
        app: {
          userInfo: state.app.userInfo,
          loginInfo: state.app.loginInfo
        }
      }
    }
  })]
})
export default store
