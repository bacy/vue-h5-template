import { encrypt, decrypt } from '../../utils/secret'

const app = {
  namespaced: true,
  state: {
    userInfo: {},
    loginInfo: {
      username: '',
      password: '',
      rememberPwd: false
    }
  },
  getters: {
    loginInfo(state) {
      return {
        username: state.loginInfo.username,
        password: decrypt(state.loginInfo.password),
        rememberPwd: state.loginInfo.rememberPwd
      }
    }
  },
  mutations: {
    setUserInfo(state, info) {
      state.userInfo = info
    },
    logout(state) {
      state.userInfo = {}
    },
    setLoginInfo(state, info) {
      state.loginInfo.username = info.username
      state.loginInfo.password = encrypt(info.password)
      state.loginInfo.rememberPwd = info.rememberPwd
    }
  }
}
export default app
