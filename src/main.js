import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Element from 'element-ui'
import './assets/styles/index.scss'
import './assets/styles/element-variables.scss'
import filter from './filters'

Vue.use(Element, { size: 'small', zIndex: 3000 })
Object.keys(filter).forEach(k => Vue.filter(k, filter[k]))

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
