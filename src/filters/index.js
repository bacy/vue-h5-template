import moment from 'moment-mini'

const filter = {
  formatMoney (value) {
    if (value) {
      value = Number(value)
      return (value / 100).toFixed(2)
    }
    return '0'
  },
  formatDate (value) {
    return moment(value).format('YYYY-MM-DD')
  }
}
export default filter
