module.exports = {
  productionSourceMap: false,
  // devServer: {
  //   host: '127.0.0.1',
  //   port: 8081,
  //   proxy: {
  //     '/api': {
  //       target: process.env.VUE_APP_BASE_URL,
  //       pathRewrite: { '^/api': '' },
  //       changeOrigin: true
  //     }
  //   }
  // },
  css: {
    loaderOptions: {
      sass: {
        // @是src的别名
        prependData: `@import "@/assets/styles/variable.scss";`
      }
    }
  }
}
